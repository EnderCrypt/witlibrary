package endercrypt.wit.ratelimit;

import java.util.concurrent.TimeUnit;

public class TimedRateLimiter implements RateLimiter
{
	private final long delay;
	private long nextFreeTime;

	public TimedRateLimiter(TimeUnit timeUnit, int duration)
	{
		this.delay = timeUnit.toMillis(duration);
	}

	@Override
	public void ratelimitBlock()
	{
		long time = System.currentTimeMillis();
		if (time >= this.nextFreeTime)
		{ // no waiting needed
			this.nextFreeTime = time + this.delay; // move forwards wait to delay
			return;
		}
		else
		{ // waiting is needed
			long wait = this.nextFreeTime - time; // how long we need to wait
			this.nextFreeTime += this.delay; // move wait needed forwards
			try
			{
				Thread.sleep(wait);
			}
			catch (InterruptedException e)
			{
				throw new RuntimeException(e);
			}
		}
	}
}
