package endercrypt.wit.exception;

import com.google.gson.JsonObject;

@SuppressWarnings("serial")
public class WitJsonException extends WitException
{
	public static void throwIfError(JsonObject json)
	{
		if (json.has("error"))
		{
			throw new WitJsonException(json);
		}
	}

	public WitJsonException(JsonObject json)
	{
		this(json.get("error").getAsString());
	}

	public WitJsonException(String message)
	{
		super(message);
	}
}
