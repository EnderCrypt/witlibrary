package endercrypt.wit.response.entity;


import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import endercrypt.wit.WitSettings;


public class EntityArray
{
	// private WitApiSettings witApiSettings;
	private String name;
	private List<Entity> entities = new ArrayList<>();
	
	public EntityArray(WitSettings settings, String name, JsonArray array)
	{
		// this.witApiSettings = witApiSettings;
		this.name = name;
		
		for (JsonElement jsonType : array)
		{
			JsonObject entityJson = jsonType.getAsJsonObject();
			Entity entity = new Entity(getName(), entityJson);
			this.entities.add(entity);
		}
		
		this.entities.removeIf((e) -> e.getConfidence() < settings.getDefaultMinimumConfidence());
		
		/*
		 * Collections.sort(this.entities, new Comparator<Entity>() {
		 * 
		 * @Override public int compare(Entity e1, Entity e2) { return
		 * Double.compare(e1.getConfidence(), e2.getConfidence()); } });
		 */
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public Entity getBest()
	{
		return this.entities.get(0);
	}
	
	public int count()
	{
		return this.entities.size();
	}
}
