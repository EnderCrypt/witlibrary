package endercrypt.wit;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import endercrypt.wit.action.Action;
import endercrypt.wit.exception.WitException;
import endercrypt.wit.exception.WitJsonException;
import endercrypt.wit.ratelimit.RateLimiter;
import endercrypt.wit.response.impl.MessageResponse;


public class Wit
{
	public static final String WIT_VERSION = "20190219";
	
	public static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	private final WitSettings settings;
	
	public Wit()
	{
		this(new WitSettings());
	}
	
	public Wit(WitSettings settings)
	{
		this.settings = settings;
	}
	
	public WitSettings getWitApiSettings()
	{
		return this.settings;
	}
	
	private Connection jsoupConnect(String endpoint, Map<String, String> queries, Map<String, String> headers)
	{
		// null checks
		Objects.requireNonNull(endpoint);
		Objects.requireNonNull(queries);
		Objects.requireNonNull(headers);
		// local variables
		Map<String, String> localQueries = new HashMap<>();
		Map<String, String> localHeaders = new HashMap<>(headers);
		try
		{
			for (Entry<String, String> entry : queries.entrySet())
			{
				String key = URLEncoder.encode(entry.getKey(), "UTF-8");
				String value = URLEncoder.encode(entry.getValue(), "UTF-8");
				localQueries.put(key, value);
			}
		}
		catch (UnsupportedEncodingException e)
		{
			throw new WitException(e);
		}
		// default values
		localQueries.put("v", WIT_VERSION);
		localHeaders.put("Authorization", "Bearer " + getWitApiSettings().getApiKey());
		localHeaders.put("Accept", "application/vnd.wit." + WIT_VERSION + "+json");
		//
		String queryString = localQueries.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("&"));
		if (queries.size() > 0)
		{
			queryString = "?" + queryString;
		}
		
		// start up our god: jsoup
		String url = "https://api.wit.ai/" + endpoint + queryString;
		Connection connection = Jsoup.connect(url);
		connection.method(Method.GET);
		connection.ignoreContentType(true);
		
		// headers'n stuff
		connection.headers(localHeaders);
		
		return connection;
	}
	
	private static void validateResponse(Response response)
	{
		if (response.statusCode() != 200) throw new WitException("api.wit.ai responded with an unexpected status code: " + response.statusCode() + (response.statusMessage() == null ? "" : " (" + response.statusMessage() + ")"));
		if (response.hasHeaderWithValue("Content-Type", "application/json; charset=UTF-8") == false) throw new WitException("api.wit.ai responded with an unexpected Content-Type: " + response.contentType());
	}
	
	private <T> Action<T> request(Connection connection, Function<JsonObject, T> callback)
	{
		return new Action<>(new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				// rate limit
				RateLimiter rateLimiter = Wit.this.settings.getRateLimiter();
				synchronized (rateLimiter)
				{
					// limit
					rateLimiter.ratelimitBlock();
					// ALRIGHT LETS DO THIS!
					Response response = connection.execute();
					// verify
					validateResponse(response);
					// json
					JsonObject json = gson.fromJson(response.body(), JsonObject.class);
					// System.out.println(gson.toJson(json));
					// error check
					WitJsonException.throwIfError(json);
					// get T
					// T returnObject = null;
					T returnObject = callback.apply(json);
					// return
					return returnObject;
				}
			}
		});
	}
	
	public Action<MessageResponse> resolveMessage(String string)
	{
		Map<String, String> queries = new HashMap<>();
		Map<String, String> headers = new HashMap<>();
		
		queries.put("q", string);
		
		return request(jsoupConnect("message", queries, headers), (json) -> new MessageResponse(this.settings, json));
	}
}
