package endercrypt.wit.action;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class Action<T>
{
	private static final ExecutorService executor = Executors.newCachedThreadPool();

	private Callable<T> callable;

	public Action(Callable<T> callable)
	{
		this.callable = callable;
	}

	public T complete()
	{
		try
		{
			return executor.submit(this.callable).get();
		}
		catch (InterruptedException | ExecutionException e)
		{
			throw new ActionException(e);
		}
	}

	public void queue(Consumer<T> listener)
	{
		queue(listener, e -> e.printStackTrace());
	}

	public void queue(Consumer<T> listener, Consumer<Exception> exceptionListener)
	{
		executor.submit(new Runnable()
		{
			@Override
			public void run()
			{
				T result = null;
				try
				{
					result = Action.this.callable.call();
				}
				catch (Exception e)
				{
					exceptionListener.accept(e);
					return;
				}
				listener.accept(result);
			}
		});
	}
}
