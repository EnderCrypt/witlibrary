package endercrypt.wit.response.impl;


import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.gson.JsonObject;

import endercrypt.wit.WitSettings;
import endercrypt.wit.exception.WitEntityMissingException;
import endercrypt.wit.response.WitAiResponse;
import endercrypt.wit.response.entity.Entity;
import endercrypt.wit.response.entity.EntityCollection;


public class MessageResponse extends WitAiResponse
{
	private final EntityCollection entities;
	
	private final String msgId;
	private final String text;
	
	public MessageResponse(WitSettings settings, JsonObject json)
	{
		super(settings);
		this.msgId = json.get("msg_id").getAsString();
		this.text = json.get("_text").getAsString();
		
		this.entities = new EntityCollection(settings, json.get("entities").getAsJsonObject());
	}
	
	// common
	
	public String getMsgId()
	{
		return this.msgId;
	}
	
	public String getText()
	{
		return this.text;
	}
	
	// intent
	public Entity requireIntent()
	{
		return require("intent");
	}
	
	public Optional<Entity> optionalIntent()
	{
		return optional("intent");
	}
	
	public Entity getIntent()
	{
		return get("intent");
	}
	
	// entity
	public Entity require(String entityType)
	{
		return optional(entityType).orElseThrow(() -> new WitEntityMissingException(entityType + " not included in wit.ai response"));
	}
	
	public Optional<Entity> optional(String entityType)
	{
		return Optional.ofNullable(get(entityType));
	}
	
	public Entity get(String entityType)
	{
		return this.entities.get(entityType);
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getName()).append('\n');
		sb.append("Msg-id: ").append(this.msgId).append('\n');
		sb.append("Text: ").append(this.text).append('\n');
		sb.append("Entities: ").append('\n');
		String tabbedEntities = Arrays.stream(this.entities.toString().split("\n")).map((s) -> "\t" + s).collect(Collectors.joining("\n"));
		sb.append(tabbedEntities);
		return sb.toString();
	}
}
