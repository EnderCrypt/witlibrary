package endercrypt.wit.exception;

@SuppressWarnings("serial")
public class WitException extends RuntimeException
{
	public WitException()
	{
		super();
	}

	public WitException(String message)
	{
		super(message);
	}

	public WitException(Throwable cause)
	{
		super(cause);
	}

	public WitException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public WitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
