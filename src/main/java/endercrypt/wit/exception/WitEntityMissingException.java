package endercrypt.wit.exception;

@SuppressWarnings("serial")
public class WitEntityMissingException extends WitException
{
	public WitEntityMissingException()
	{
		super();
	}

	public WitEntityMissingException(String message)
	{
		super(message);
	}

	public WitEntityMissingException(Throwable cause)
	{
		super(cause);
	}

	public WitEntityMissingException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public WitEntityMissingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
