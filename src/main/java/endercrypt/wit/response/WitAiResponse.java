package endercrypt.wit.response;

import endercrypt.wit.WitSettings;

public class WitAiResponse
{
	private WitSettings settings;
	// private JsonObject json;

	public WitAiResponse(WitSettings settings)
	{
		this.settings = settings;
		// this.json = json;
	}

	public WitSettings getSettings()
	{
		return this.settings;
	}

	/*
	public JsonObject getJson()
	{
		return this.json;
	}
	*/
}
