package test;


import java.util.Scanner;

import endercrypt.wit.Wit;
import endercrypt.wit.WitSettings;
import endercrypt.wit.response.impl.MessageResponse;


public class Main
{
	private static final Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) throws InterruptedException
	{
		// api
		Wit wit = new Wit();
		
		// setup
		WitSettings witSetting = wit.getWitApiSettings();
		witSetting.setApiKey(args[0]);
		witSetting.setDefaultMinimumConfidence(0.75);
		
		// wit.ai
		while (true)
		{
			System.out.print("> ");
			String command = scanner.nextLine();
			MessageResponse response = wit.resolveMessage(command).complete();
			
			String intent = response.optionalIntent().map(i -> i.getValue()).orElse(null);
			System.out.println("Intent: " + intent);
			
			String name = response.optional("name").map(e -> e.getValue()).orElse(null);
			System.out.println("Name: " + name);
			
			String path = response.optional("path").map(e -> e.getValue()).orElse(null);
			System.out.println("Path: " + path);
		}
	}
}
