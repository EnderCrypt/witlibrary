package endercrypt.wit.response.entity;


import com.google.gson.JsonObject;


public class Entity
{
	private final String entityType;
	
	private final double confidence;
	private final String value;
	
	public Entity(String entityType, JsonObject json)
	{
		this.entityType = entityType;
		
		this.confidence = json.get("confidence").getAsDouble();
		this.value = json.get("value").getAsString();
	}
	
	public String getEntityType()
	{
		return this.entityType;
	}
	
	public double getConfidence()
	{
		return this.confidence;
	}
	
	public String getValue()
	{
		return this.value;
	}
}
