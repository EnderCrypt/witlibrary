package endercrypt.wit.ratelimit;

public interface RateLimiter
{
	void ratelimitBlock();
}
