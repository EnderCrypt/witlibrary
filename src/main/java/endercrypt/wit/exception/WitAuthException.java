package endercrypt.wit.exception;

@SuppressWarnings("serial")
public class WitAuthException extends WitException
{
	public WitAuthException()
	{
		super();
	}

	public WitAuthException(String message)
	{
		super(message);
	}

	public WitAuthException(Throwable cause)
	{
		super(cause);
	}

	public WitAuthException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public WitAuthException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
