package endercrypt.wit.response.entity;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import endercrypt.wit.WitSettings;


public class EntityCollection
{
	private Map<String, EntityArray> map = new HashMap<>();
	
	public EntityCollection(WitSettings settings, JsonObject json)
	{
		for (Entry<String, JsonElement> entry : json.entrySet())
		{
			String key = entry.getKey();
			JsonArray value = entry.getValue().getAsJsonArray().getAsJsonArray();
			EntityArray entityArray = new EntityArray(settings, key, value);
			if (entityArray.count() > 0)
			{
				this.map.put(key, entityArray);
			}
		}
	}
	
	// entity
	public Entity get(String entityType)
	{
		EntityArray entityArray = this.map.get(entityType);
		if (entityArray != null)
		{
			return entityArray.getBest();
		}
		return null;
	}
	
	// types
	public Set<String> getTypes()
	{
		return Collections.unmodifiableSet(this.map.keySet());
	}
	
	// to string
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for (Entry<String, EntityArray> entry : this.map.entrySet())
		{
			sb.append(entry.getKey());
			sb.append(": ");
			sb.append(entry.getValue());
			sb.append('\n');
		}
		if (sb.length() > 0)
		{
			sb.setLength(sb.length() - 1);
		}
		return sb.toString();
	}
}
