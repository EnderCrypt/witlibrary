package endercrypt.wit;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import endercrypt.wit.ratelimit.NoRateLimiter;
import endercrypt.wit.ratelimit.RateLimiter;
import endercrypt.wit.ratelimit.TimedRateLimiter;

public class WitSettings
{
	public WitSettings()
	{
		setRateLimiter(new TimedRateLimiter(TimeUnit.SECONDS, 1));
		setDefaultMinimumConfidence(0.75);
	}

	// API KEY //
	private String apiKey;

	public String getApiKey()
	{
		return this.apiKey;
	}

	public void setApiKey(String apiKey)
	{
		this.apiKey = apiKey;
	}

	// RATE LIMITER //
	private static final RateLimiter defaultRatelimiter = new NoRateLimiter();
	private RateLimiter rateLimiter;

	public RateLimiter getRateLimiter()
	{
		if (this.rateLimiter == null)
		{
			return defaultRatelimiter;
		}
		return this.rateLimiter;
	}

	public void setRateLimiter(RateLimiter rateLimiter)
	{
		Objects.requireNonNull(rateLimiter);
		this.rateLimiter = rateLimiter;
	}

	// MINIMUM CONFIDENCE //
	private double defaultMinimumConfidence;

	public void setDefaultMinimumConfidence(double defaultMinimumConfidence)
	{
		if (defaultMinimumConfidence < 0.0 || defaultMinimumConfidence > 1.0)
		{
			throw new IllegalArgumentException("defaultMinimumConfidence should be from 0 to 1");
		}
		this.defaultMinimumConfidence = defaultMinimumConfidence;
	}

	public double getDefaultMinimumConfidence()
	{
		return this.defaultMinimumConfidence;
	}
}
